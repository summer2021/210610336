import sys
import mindspore

from model_zoo.official.cv.resnet.src.resnet import resnet50, resnet18,resnet34,resnet101,resnet152
from model_zoo.official.cv.vgg16.src.vgg import vgg16
from model_zoo.official.cv.vgg16.model_utils.moxing_adapter import config
from model_zoo.official.cv.densenet.src.network.densenet import _densenet121,_densenet161,_densenet169,_densenet201
from model_zoo.official.cv.squeezenet.src.squeezenet import SqueezeNet,SqueezeNet_Residual
from model_zoo.official.cv.shufflenetv1.src.shufflenetv1 import ShuffleNetV1
from model_zoo.official.cv.shufflenetv2.src.shufflenetv2 import ShuffleNetV2
from model_zoo.official.cv.efficientnet.src.efficientnet import efficientnet_b0
from msflops import stat
from models import alexnet
from mindspore import context
ost = sys.stdout

if __name__ == '__main__':
    """
    AlexNet
    """
    # model = alexnet.AlexNet()
    """
    Resnet
    """
    # model = resnet50()
    """
    Vgg
    """
    # model = vgg16(num_classes=config.num_classes, args=config)
    """
    Densenet
    """
    # model = _densenet201()
    """
    SqueezeNet
    """
    # model = SqueezeNet_Residual()
    """
    ShuffleNet
    """
    # model = ShuffleNetV2()
    """
    efficientnetb0
    """
    model = efficientnet_b0()
    context.set_context(mode=context.PYNATIVE_MODE)
    stat(model, (3, 224, 224))
    # for cell in model.cells_and_names():
    #     print(cell)